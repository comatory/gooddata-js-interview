// @flow

import React from 'react'

import { AppContext } from './AppContextProvider'
import MonthlyColumnChart from './MonthlyColumnChart'

type Props = {|
    month: string,
|}

// NOTE: I chose to create this container because this stack is on
//       React v16.5.xx which means I cannot use static property
//       `contextType` on components itself otherwise this container
//       (and the one for `YearlyColumnChart` did not have to exist)
export default class MonthlyColumnChartContainer extends React.PureComponent<Props> {
    render() {
        return (
            <AppContext.Consumer>
                {({
                    grossProfitMeasure,
                    dateAttributeInMonths,
                    dateAttribute,
                    projectId,
                }) => (
                    <MonthlyColumnChart
                        month={this.props.month}
                        grossProfitMeasure={grossProfitMeasure}
                        dateAttributeInMonths={dateAttributeInMonths}
                        dateAttribute={dateAttribute}
                        projectId={projectId}
                    />
                )}
            </AppContext.Consumer>
        )
    }
}

