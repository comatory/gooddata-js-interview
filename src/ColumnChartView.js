// @flow

import React from 'react'
import { ColumnChart } from '@gooddata/react-components';

import type { Measure, Filter, View } from './types'

type Props = {|
    filters?: Array<Filter>,
    measures: Array<Measure>,
    projectId: string,
    viewBy: View,
|}

export default class ColumnChartView extends React.PureComponent<Props> {
    render() {
        return (
            <ColumnChart
                measures={this.props.measures}
                filters={this.props.filters}
                projectId={this.props.projectId}
                viewBy={this.props.viewBy}
            />
        )
    }
}
