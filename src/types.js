export type Filter = {
    absoluteDateFilter: {
        dataSet: {
            uri: string,
        },
        from: string,
        to: string,
    }
}

export type Measure = {
    measure: {
        localIdentifier: string,
        definition: {
            measureDefinition: {
                item: {
                    uri: string,
                },
            },
        },
    }
}

export type View = {
    visualizationAttribute: {
        displayForm: {
            uri: string,
        },
    },
}

