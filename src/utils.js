// @flow

export default class Utils {
    static getLastDayOfMonth(year: number, month: string): string {
        // NOTE: `month` in this case is not "index"
        //       e.g. this means 1 = Jan, 2 = Feb... 12 = Dec
        const date = new Date(year, Number(month), 0)

        // NOTE: This can probably never happen but make
        //       sure day if always two digits
        return String(date.getDate()).padStart(2, '0')
    }
}

