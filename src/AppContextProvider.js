// @flow

import React from 'react'

export type AppContextProviderValue = {|
    grossProfitMeasure: string,
    dateAttributeInMonths: string,
    dateAttribute: string,
    projectId: string,
|}

const baseContextValue = {
    grossProfitMeasure: process.env.REACT_APP_grossProfitMeasure || '',
    dateAttributeInMonths: process.env.REACT_APP_dateAttributeInMonths || '',
    dateAttribute: process.env.REACT_APP_dateAttribute || '',
    projectId: process.env.REACT_APP_projectId || '',
}

const AppContext: React$Context<AppContextProviderValue> = React.createContext({
    grossProfitMeasure: '',
    dateAttributeInMonths: '',
    dateAttribute: '',
    projectId: '',
})

type Props = {|
    children: React$Node | Array<React$Node>,
|}

export default class AppContextProvider extends React.PureComponent<Props> {
    render() {
        return (
            <AppContext.Provider value={baseContextValue} >
                {this.props.children}
            </AppContext.Provider>
        )
    }
}

export { AppContext }
