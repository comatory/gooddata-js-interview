// @flow

import React from 'react'

type Option = {|
    value: string | number | null,
    label: string,
|}

type Props = {|
    options: Array<Option>,
    value: string | number | null,
    name: string,
    onChange?: (value: string | number | null) => any,
|}

type State = {|
    value: string | number | null,
|}

export default class Select extends React.PureComponent<Props, State> {
    static defaultProps = {
        options: [],
        value: null,
        name: '',
    }

    UNSAFE_componentWillReceiveProps(nextProps: Props) {
        if (this.props.value !== nextProps.value) {
            this.setState({
                value: nextProps.value,
            })
        }
    }

    state = {
        value: this.props.value,
    }

    _handleSelectChange = (proxy: SyntheticInputEvent<*>) => {
        const value = proxy.target.value

        this.setState({ value }, () => {
            if (this.props.onChange) {
                this.props.onChange(value)
            }
        })
    }

    render() {
        const { name, options } = this.props
        const { value } = this.state

        return (
            <select
                value={value}
                onChange={this._handleSelectChange}
            >
                {options.map(({ value, label }) => (
                    <option key={`${name}-${value || ''}`} value={value}>{label}</option>
                ))}
            </select>
        )
    }
}
