// @flow

import React from 'react'

import ColumnChartContainer from './ColumnChartContainer'

import type { View } from './types'

export type Props = {|
    projectId: string,
    grossProfitMeasure: string,
    dateAttributeInMonths: string,
    dateAttribute: string,
|}

export default class YearlyColumnChart extends React.PureComponent<Props> {
    getViewBy(): View {
        return {
            visualizationAttribute:
            {
                displayForm: {
                    uri: this.props.dateAttributeInMonths
                },
                localIdentifier: 'a1'
            }
        }
    }

    render() {
        const viewBy = this.getViewBy();

        return (
            <ColumnChartContainer
              projectId={this.props.projectId}
              grossProfitMeasure={this.props.grossProfitMeasure}
              dateAttributeInMonths={this.props.dateAttributeInMonths}
              dateAttribute={this.props.dateAttribute}
              viewBy={viewBy}
            />
        )
    }
}
