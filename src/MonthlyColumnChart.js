// @flow

import React from 'react'
import ColumnChartContainer from './ColumnChartContainer'
import Utils from './utils'

import type { Filter } from './types'

export type Props = {|
    projectId: string,
    month: string,
    grossProfitMeasure: string,
    dateAttributeInMonths: string,
    dateAttribute: string,
|}

export default class MonthlyColumnChart extends React.PureComponent<Props> {

    getMonthFilter(): Filter {
        const from = `2016-${this.props.month}-01`

        const lastDayOfMonth = Utils.getLastDayOfMonth(2016, this.props.month)
        const to = `2016-${this.props.month}-${lastDayOfMonth}`

        return {
            absoluteDateFilter: {
                dataSet: {
                    uri: this.props.dateAttribute
                },
                from,
                to,
            }

        }
    }

    render() {
        const filters = [this.getMonthFilter()];

        return (
            <ColumnChartContainer
                projectId={this.props.projectId}
                grossProfitMeasure={this.props.grossProfitMeasure}
                dateAttributeInMonths={this.props.dateAttributeInMonths}
                dateAttribute={this.props.dateAttribute}
                filters={filters}
            />
        )
    }
}

