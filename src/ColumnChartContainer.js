// @flow

import React from 'react'

import ColumnChartView from './ColumnChartView'

import type { Filter, Measure, View } from './types'

type Props = {|
    projectId: string,
    filters?: Array<Filter>,
    viewBy?: View,
    dateAttribute: string,
    dateAttributeInMonths: string,
    grossProfitMeasure: string,
|}

export default class ColumnChartContainer extends React.PureComponent<Props> {

    getMeasures(): Array<Measure> {
        return [
            {
                measure: {
                    localIdentifier: 'm1',
                    definition: {
                        measureDefinition: {
                            item: {
                                uri: this.props.grossProfitMeasure
                            }
                        }
                    },
                    alias: '$ Gross Profit'
                }
            }
        ]
    }

    render() {
        const measures = this.getMeasures();

        return (
            <ColumnChartView
                measures={measures}
                filters={this.props.filters}
                projectId={this.props.projectId}
                viewBy={this.props.viewBy}
            />
        )
    }
}
