// @flow
// Copyright (C) 2007-2019, GoodData(R) Corporation. All rights reserved.

import React, { Component } from 'react';
import '@gooddata/react-components/styles/css/main.css';

import MonthlyColumnChartContainer from './MonthlyColumnChartContainer'
import YearlyColumnChartContainer from './YearlyColumnChartContainer'
import MonthDropdown from './MonthDropdown'
import AppContextProvider from './AppContextProvider'

type State = {|
  month: string,
|}

class App extends Component<{}, State> {
    state = {
      month: "1",
    }

    _handleDropdownChange = (value: string | number | null) => {
      this.setState({
        month: String(value),
      })
    }

    render() {
      const { month } = this.state

        return (
            <AppContextProvider>
                <div className="App">
                    <h1>$ Gross Profit in month <MonthDropdown month={month} onChange={this._handleDropdownChange} /> 2016</h1>
                    <div>
                      <MonthlyColumnChartContainer month={month} />
                    </div>
                    <h1>$ Gross Profit - All months</h1>
                    <div>
                      <YearlyColumnChartContainer />
                    </div>
                </div>
            </AppContextProvider>
        );
    }
}

export default App;
