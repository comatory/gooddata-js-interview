// @flow

import React from 'react'
import { shallow } from 'enzyme'
import chai, { expect } from 'chai'
import chaiEnzyme from 'chai-enzyme'
import { v4 } from 'uuid'

import ColumnChartView from '../ColumnChartView'


chai.use(chaiEnzyme())

describe('ColumnChartView', () => {
    const renderComponent = (props) => {
        const defaultProps = {
            measures: [],
            projectId: v4(),
            viewBy: {
                visualizationAttribute: {
                    displayForm: {
                        uri: v4(),
                    },
                },
            }
        }

        return shallow(
            <ColumnChartView
                {...defaultProps}
                {...props}
            />
        )
    }


    it('should render column chart', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('ColumnChart'))
           .to.exist
    })


    it('should pass measures to column chart', () => {
        const measures = [
            {
                measure: {
                    localIdentifier: 'x',
                    definition: {
                        measureDefinition: {
                            item: {
                                uri: '/xyz/xxx',
                            },
                        },
                    },
                }
            }
        ]
        const wrapper = renderComponent({ measures })

        expect(wrapper.find('ColumnChart').props().measures)
           .equal(measures)
    })


    it('should pass filters to column chart', () => {
        const filters = [
            {
                absoluteDateFilter: {
                    dataSet: {
                        uri: 'xyz',
                    },
                    from: '2018-01-01',
                    to: '2018-01-21',
                }
            },
        ]
        const wrapper = renderComponent({ filters })

        expect(wrapper.find('ColumnChart').props().filters)
           .equal(filters)
    })


    it('should pass project ID to column chart', () => {
        const wrapper = renderComponent({ projectId: 'abcd' })

        expect(wrapper.find('ColumnChart').props().projectId)
           .equal('abcd')
    })


    it('should pass view by to column chart', () => {
        const viewBy = {
            visualizationAttribute: {
                displayForm: {
                    uri: '/yyy/xxx',
                },
            }
        }
        const wrapper = renderComponent({ viewBy })

        expect(wrapper.find('ColumnChart').props().viewBy)
           .equal(viewBy)
    })
})
