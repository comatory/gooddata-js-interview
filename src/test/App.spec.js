// @flow

import React from 'react'
import { shallow } from 'enzyme'
import chai, { expect } from 'chai'
import chaiEnzyme from 'chai-enzyme'

import App from '../App'


chai.use(chaiEnzyme())

describe('App', () => {
    const renderComponent = () => {
        return shallow(<App />)
    }


    it('should render app context provider', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('AppContextProvider'))
            .to.exist
    })


    it('should render app wrapper', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('.App'))
            .to.exist
    })


    it('should render month dropdown', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('MonthDropdown'))
            .to.exist
    })


    it('should render month column chart container', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('MonthlyColumnChartContainer'))
            .to.exist
    })


    it('should pass january as default month to ' +
       'monthly column chart container', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('MonthlyColumnChartContainer').props().month)
            .to.equal('1')
    })


    it('should render yearly column chart container', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('YearlyColumnChartContainer'))
            .to.exist
    })


    it('should handle month change via dropdown', () => {
        const wrapper = renderComponent()

        wrapper.find('MonthDropdown').props().onChange('3')

        expect(wrapper.find('MonthlyColumnChartContainer').props().month)
          .to.equal('3')
    })
})

