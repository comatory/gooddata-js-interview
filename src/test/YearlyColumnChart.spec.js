// @flow

import React from 'react'
import { shallow } from 'enzyme'
import chai, { expect } from 'chai'
import chaiEnzyme from 'chai-enzyme'
import { v4 } from 'uuid'

import YearlyColumnChart, { type Props } from '../YearlyColumnChart'


chai.use(chaiEnzyme())

describe('YearlyColumnChart', () => {
    const renderComponent = (props: $Shape<Props> = {}) => {
        const defaultProps = {
            projectId: v4(),
            grossProfitMeasure: v4(),
            dateAttributeInMonths: v4(),
            dateAttribute: v4(),
        }

        return shallow(
            <YearlyColumnChart
                {...defaultProps}
                {...props}
            />
        )
    }


    it('should render column chart container', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('ColumnChartContainer'))
             .to.exist
    })


    it('should pass project ID to column chart container', () => {
        const wrapper = renderComponent({
            projectId: 'abc',
        })

        expect(wrapper.find('ColumnChartContainer').props().projectId)
             .to.equal('abc')
    })


    it('should pass gross profit measure to column chart container', () => {
        const wrapper = renderComponent({
            grossProfitMeasure: '/gross/123',
        })

        expect(wrapper.find('ColumnChartContainer').props().grossProfitMeasure)
             .to.equal('/gross/123')
    })


    it('should pass date attribute in months to column chart container', () => {
        const wrapper = renderComponent({
            dateAttributeInMonths: '/date-months/123',
        })

        expect(wrapper.find('ColumnChartContainer').props().dateAttributeInMonths)
             .to.equal('/date-months/123')
    })


    it('should pass date attribute to column chart container', () => {
        const wrapper = renderComponent({
            dateAttribute: '/date/123',
        })

        expect(wrapper.find('ColumnChartContainer').props().dateAttribute)
             .to.equal('/date/123')
    })


    it('should create and pass view by to column chart container', () => {
        const wrapper = renderComponent({
            dateAttributeInMonths: '/date-month/xyz',
        })

        expect(wrapper.find('ColumnChartContainer').props().viewBy)
          .to.deep.equal({
               visualizationAttribute: {
                   displayForm: {
                       uri: '/date-month/xyz',
                   },
                   localIdentifier: 'a1'
               }
        })
    })
})
