// @flow

import React from 'react'
import { shallow } from 'enzyme'
import chai, { expect } from 'chai'
import chaiEnzyme from 'chai-enzyme'
import { v4 } from 'uuid'

import ColumnChartContainer from '../ColumnChartContainer'


chai.use(chaiEnzyme())

describe('ColumnChartContainer', () => {
    const renderComponent = (props) => {
        // NOTE: I am using these to run each spec
        //       with randomized values. This component
        //       is not very complex so it's not needed
        //       but it is quite useful for more complex
        //       components
        const defaultProps = {
            projectId: v4(),
            dateAttribute: v4(),
            dateAttributeInMonths: v4(),
            grossProfitMeasure: v4(),
        }

        return shallow(
            <ColumnChartContainer
                {...defaultProps}
                {...props}
            />
        )
    }

    it('should render column chart view', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('ColumnChartView'))
             .to.exist
    })


    it('should create and pass measures to column ' +
        'chart view', () => {
        const wrapper = renderComponent({
            grossProfitMeasure: '/xyz/abcd',
        })

        const columnChartViewProps = wrapper.find('ColumnChartView').props()

        expect(columnChartViewProps.measures).to.have.length(1)
        expect(columnChartViewProps.measures[0]).to.deep.equal({
            measure: {
                localIdentifier: 'm1',
                definition: {
                    measureDefinition: {
                        item: {
                            uri: '/xyz/abcd',
                        }
                    }
                },
                alias: '$ Gross Profit',
            }
        })
    })


    it('should pass filters prop to column chart view', () => {
        const filters = [
            {
                absoluteDateFilter: {
                    dataSet: {
                        uri: 'xyz',
                    },
                    from: '2018-01-01',
                    to: '2018-01-21',
                }
            },
        ]
        const wrapper = renderComponent({
            filters,
        })

        expect(wrapper.find('ColumnChartView').props().filters)
             .to.equal(filters)
    })


    it('should pass project ID to column chart view', () => {
        const wrapper = renderComponent({
            projectId: 'abcd',
        })

        expect(wrapper.find('ColumnChartView').props().projectId)
            .to.equal('abcd')
    })


    it('should pass viewBy to column chart view', () => {
        const viewBy = {
            visualizationAttribute: {
                displayForm: {
                    uri: '/yyy/xxx',
                },
            }
        }
        const wrapper = renderComponent({
            viewBy,
        })

        expect(wrapper.find('ColumnChartView').props().viewBy)
            .to.equal(viewBy)
    })
})
