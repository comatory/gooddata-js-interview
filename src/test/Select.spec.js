// @flow

import React from 'react'
import { mount, shallow } from 'enzyme'
import chai, { expect } from 'chai'
import chaiEnzyme from 'chai-enzyme'
import sinon from 'sinon'
import chaiSinon from 'sinon-chai'

import Select from '../Select'


chai.use(chaiEnzyme())
chai.use(chaiSinon)

describe('Select', () => {
    const renderComponent = (props = {}) => {
        return shallow(
            <Select {...props} />
        )
    }

    const renderMountedComponent = (props = {}) => {
        return mount(
            <Select {...props} />
        )
    }

    it('should render select', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('select'))
            .to.exist
    })


    it('should render default null value for select', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('select').props().value)
            .to.be.null
    })


    it('should render no options by default', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('option'))
            .not.to.exist
    })


    it('should render select with options', () => {
        const wrapper = renderComponent({
            options: [
                { value: 'a', label: 'abc' },
                { value: 'x', label: 'xyz' },
            ],
        })

        expect(wrapper.find('option[value="a"]'))
          .to.exist
        expect(wrapper.find('option[value="x"]'))
          .to.exist
    })


    it('should render select with labeled options', () => {
        const wrapper = renderComponent({
            options: [
                { value: 'a', label: 'abc' },
                { value: 'x', label: 'xyz' },
            ],
        })

        expect(wrapper.find('option[value="a"]').text())
          .to.equal('abc')
        expect(wrapper.find('option[value="x"]').text())
          .to.equal('xyz')
    })


    it('should set value on select for given option', () => {
        const wrapper = renderMountedComponent({
            options: [
                { value: 'a', label: 'abc' },
                { value: 'x', label: 'xyz' },
            ],
        })

        wrapper.find('option[value="x"]').simulate('change')

        expect(wrapper.find('select').props().value)
          .to.equal('x')
    })


    it('should call onChange callback when option is selected', () => {
        const onChange = sinon.spy()
        const wrapper = renderMountedComponent({
            options: [
                { value: 'a', label: 'abc' },
                { value: 'x', label: 'xyz' },
            ],
            onChange,
        })

        wrapper.find('option[value="x"]').simulate('change')

        expect(onChange).to.have.been.calledWith('x')
    })


    it('should render with selected option', () => {
        const wrapper = renderComponent({
            options: [
                { value: 'a', label: 'abc' },
                { value: 'x', label: 'xyz' },
            ],
            value: 'x',
        })

        expect(wrapper.find('select').props().value)
             .to.equal('x')
    })


    it('should update selected option via "value" prop', () => {
        const wrapper = renderMountedComponent({
            options: [
                { value: 'a', label: 'abc' },
                { value: 'x', label: 'xyz' },
            ],
        })

        wrapper.find('option[value="x"]').simulate('change')

        wrapper.setProps({
            value: 'a',
        })

        expect(wrapper.find('select').props().value)
            .to.equal('a')
    })
})

