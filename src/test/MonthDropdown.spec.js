// @flow

import React from 'react'
import { shallow } from 'enzyme'
import chai, { expect } from 'chai'
import chaiEnzyme from 'chai-enzyme'
import sinon from 'sinon'
import chaiSinon from 'sinon-chai'

import MonthDropdown from '../MonthDropdown'


chai.use(chaiEnzyme())
chai.use(chaiSinon)

describe('MonthDropdown', () => {
    const renderComponent = (props) => {
        const defaultProps = {
            month: ['1', '2', '3'][Math.round(Math.random() * 3)],
            onChange: sinon.spy(),
        }

        return shallow(
            <MonthDropdown
                {...defaultProps}
                {...props}
            />
        )
    }


    it('should render select component', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('Select'))
            .to.exist
    })


    it('should pass month prop to select', () => {
        const wrapper = renderComponent({
            month: '3',
        })

        expect(wrapper.find('Select').props().value)
            .to.equal('3')
    })


    it('should pass fixed set of options to ' +
       'select component', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('Select').props().options)
             .to.have.length(12)
    })


    it('should pass onChange callback to select component', () => {
        const onChange = sinon.spy()
        const wrapper = renderComponent({ onChange })

        expect(wrapper.find('Select').props().onChange)
            .to.equal(onChange)
    })


    it('should pass fixed name prop to select component', () => {
        const onChange = sinon.spy()
        const wrapper = renderComponent({ onChange })

        expect(wrapper.find('Select').props().name)
            .to.equal('month-dropdown')
    })
})

