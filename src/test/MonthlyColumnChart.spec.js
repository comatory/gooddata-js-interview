// @flow

import React from 'react'
import { shallow } from 'enzyme'
import chai, { expect } from 'chai'
import chaiEnzyme from 'chai-enzyme'
import { v4 } from 'uuid'

import MonthlyColumnChart, { type Props } from '../MonthlyColumnChart'


chai.use(chaiEnzyme())

describe('MonthlyColumnChart', () => {
    const renderComponent = (props: $Shape<Props> = {}) => {
        const defaultProps = {
            projectId: v4(),
            month: ['1', '2', '3'][Math.round(Math.random() * 3)],
            grossProfitMeasure: v4(),
            dateAttributeInMonths: v4(),
            dateAttribute: v4(),
        }

        return shallow(
            <MonthlyColumnChart
                {...defaultProps}
                {...props}
            />
        )
    }

    it('should render column chart container', () => {
        const wrapper = renderComponent()

        expect(wrapper.find('ColumnChartContainer'))
            .to.exist
    })


    it('should pass gross profit measure to chart container', () => {
        const wrapper = renderComponent({
            grossProfitMeasure: '/gross/abc',
        })

        expect(wrapper.find('ColumnChartContainer').props().grossProfitMeasure)
            .to.equal('/gross/abc')
    })


    it('should pass date attribute in months to chart container', () => {
        const wrapper = renderComponent({
            dateAttributeInMonths: '/date-months/abc',
        })

        expect(wrapper.find('ColumnChartContainer').props().dateAttributeInMonths)
            .to.equal('/date-months/abc')
    })


    it('should pass date attribute to chart container', () => {
        const wrapper = renderComponent({
            dateAttribute: '/date/abc',
        })

        expect(wrapper.find('ColumnChartContainer').props().dateAttribute)
            .to.equal('/date/abc')
    })


    it('should pass project ID to chart container', () => {
        const wrapper = renderComponent({
            projectId: 'abc',
        })

        expect(wrapper.find('ColumnChartContainer').props().projectId)
            .to.equal('abc')
    })


    it('should create filter and pass it to column chart container', () => {
        const wrapper = renderComponent({
            dateAttribute: '/date/123/xyz',
            month: '3',
        })

        const filters = wrapper.find('ColumnChartContainer').props().filters

        expect(filters).to.have.length(1)
        expect(filters[0]).to.deep.equal({
            absoluteDateFilter: {
                dataSet: {
                    uri: '/date/123/xyz',
                },
                from: '2016-3-01',
                to: '2016-3-31',
            }
        })
    })
})
