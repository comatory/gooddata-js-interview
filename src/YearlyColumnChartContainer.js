// @flow

import React from 'react'

import { AppContext } from './AppContextProvider'
import YearlyColumnChart from './YearlyColumnChart'

export default class YearlyColumnChartContainer extends React.PureComponent<{}> {
    render() {
        return (
            <AppContext.Consumer>
                {({
                    grossProfitMeasure,
                    dateAttributeInMonths,
                    dateAttribute,
                    projectId,
                }) => (
                    <YearlyColumnChart
                        grossProfitMeasure={grossProfitMeasure}
                        dateAttributeInMonths={dateAttributeInMonths}
                        dateAttribute={dateAttribute}
                        projectId={projectId}
                    />
                )}
            </AppContext.Consumer>
        )
    }
}

